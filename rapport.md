# NoSQL Project Report 

*This is Kiruban PREMKUMAR's school project report. Here I will explain the modelisation choice and queries' implentations.*

## Modelisation

First I chose to separate both users and movies as two collections. Because I can access users, for login, displaying users informations or iterating through users.

Thus, I can also display movies, search movies, or see details of a specific movie. As an administrator I can also add movies and iterate through all the movies in the movie collection.

This is not a global solution, but it's perfect for my needs and project requirements.

### Users

	{
		"_id" : ObjectId("52e95c53eda1c682d0272487"),
		"admin" : false,
		"created" : 1391025235423,
		"email" : "claude@gmail.com",
		"movies" : [
			{
				"date" : ISODate("2014-01-29T20:04:56.540Z"),
				"movie" : "52e95c53eda1c682d0272476",
				"genre" : "Action"
			},
			{
				"date" : ISODate("2014-01-29T20:04:58.399Z"),
				"movie" : "52e95c53eda1c682d027247f",
				"genre" : "Action"
			},
			{
				"date" : ISODate("2014-01-29T20:05:00.123Z"),
				"movie" : "52e95c53eda1c682d027247b",
				"genre" : "Action"
			},
			{
				"date" : ISODate("2014-01-29T20:05:01.281Z"),
				"movie" : "52e95c53eda1c682d027247a",
				"genre" : "Comedie"
			},
			{
				"date" : ISODate("2014-01-29T20:05:03.484Z"),
				"movie" : "52e95c53eda1c682d0272479",
				"genre" : "Action"
			}
		],
		"password" : "claude",
		"username" : "claude"
	}

### Movies

	{
		"_id" : ObjectId("52e95c53eda1c682d0272478"),
		"acteurs" : [
			"Mark Wahlberg",
			"Mila Cunis",
			""
		],
		"annee_sortie" : 2012,
		"created" : 1391025235417,
		"genre" : "Comedie",
		"langue" : "Anglais",
		"pays_production" : "Etats-Unis",
		"producteurs" : [
			"Media Rights Capital",
			"Smart Entertainment"
		],
		"realisateur" : [
			"Seth MacFarlane"
		],
		"scenaristes" : [
			"Seth MacFarlane",
			"Alec Sulkin",
			"Wellesley Wild",
			"Seth MacFarlane"
		],
		"synopsys" : "En 1985, John Bennet, un garçon de huit ans, n'arrive pas à se faire d'amis. Le soir de Noël, il fait le vœu que son ours en peluche qu'il vient de recevoir à Noël prenne vie et qu'il soit son meilleur ami pour la vie ; son vœu sera exaucé par magie, l'ours Ted (traduction de « Teddy bear ») prend vie et cette histoire devient célèbre",
		"titre" : "Ted",
		"users" : [
			{
				"date" : ISODate("2014-01-29T20:03:46.501Z"),
				"user" : "52e95c53eda1c682d0272486"
			},
			{
				"date" : ISODate("2014-01-29T20:05:19.013Z"),
				"user" : "52e95c53eda1c682d0272485"
			}
		]
	}

## Our Queries

### Search

I build the query dynamically from the form post data :

    if(req.body.title != ''){
	    query['titre'] = {};
	    query['titre']['$regex'] = '.*' + req.body.title + '.*';
	    query['titre']['$options'] = 'i';
    }
    if(req.body.date_start != '--' && req.body.date_end != '--'){
        query['annee_sortie'] =  {};
        query['annee_sortie']['$gte'] = parseInt(req.body.date_start);
        query['annee_sortie']['$lte'] = parseInt(req.body.date_end);
    }

    if(req.body.genre != '--'){
        query['genre'] =  {};
        query['genre']['$regex'] = req.body.genre;
    }

    if(req.body.persons != ''){
        var persons = req.body.persons.split(',');
        query['$or'] =  [];
        query['$or'].push({"realisateur": { $in: persons } });
        query['$or'].push({"acteurs": { $in: persons } });
        query['$or'].push({"producteurs": { $in: persons } } );
        query['$or'].push({"scenaristes": { $in: persons } } );
    }

- I use regex for case insentive find for the title. 
- I use $gte (greater than or equal) and $lte (lesser than or equal) operators for date interval comparaison.
- I use regex too for genre.
- And finally I use $or operator to search people in all the fields (realisateur, acteurs, producteurs, scenaristes)


### Map Reduce

I used mapReduce to get the 5 most bought movies in the current month.

	db.collection('movies', function(err, collection) {
		var map = function() { emit(this.titre, this.users.length); };
    	var reduce = function(titre, count) { return Array.sum(count); };
		collection.mapReduce(
		    map,
		    reduce,
		    {
		        query: { "users.date": { $gte: start, $lte: end } },
		        out : "tmpStat1"
		    },
		    function(err, results) {
	    		db.collection('tmpStat1', function(err, collection) {
			        collection.find().sort({value:-1}).limit(5).toArray(function(err, stat) {
			           	stat1 = stat;
			        });
			    });
			});
		});

- I use the $gte and $lte operation to get the top 5 movies of the current month
- I use title as key and sum their counts to get the top 5 most bought movies.
- I output the result to the tmpStat1 collection
- Then I sort the results to get the top 5

I also used mapReduce to get the top genre of the month.

	db.collection('movies', function(err, collection) {
		var map = function() { emit(this.genre, this.users.length); };
    	var reduce = function(genre, nb) { return Array.sum(nb); };
		collection.mapReduce(
		    map,
		    reduce,
		    {
		        query: { "users.date": { $gte: start, $lte: end } },
		        out : "tmpStat2"
		    },
		    function(err, results) {
		    	console.log(err);
		    	console.log(results);
	           	db.collection('tmpStat2', function(err, collection) {
			        collection.find().sort({value:-1}).limit(1).toArray(function(err, stat) {
			           	stat2 = stat;
			        });
			    });
			});
		});

- I use the $gte and $lte operation to get the genre of the current month
- I use 'genre' as key and sum their occurences to get the top genre of the month.
- I output the result to the tmpStat2 collection
- Then I sort the results to get one result, the higher one.


## Conclusion

This project was really nice, NodeJs and mongoDB communities are growing very fastly. Moreover learning latest technologies was interesting. However it's was really hard to code using javascript, the asynchronous behaviour of javascript and callbacks everywhere is a bit messy.
I didn't found out how to implement the two last stats query, it was a bit complex, I tried to do some join but it was not recommended, then I tried doing group, then agreggate, then I tried to do more complex mapReduce but I failed too.

If I have some free times, I will try more stuff on mongodb. :)





