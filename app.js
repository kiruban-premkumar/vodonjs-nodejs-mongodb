/**
 * File : app.js
 * Subject : IBD2
 * Project : NOSQL
 * Technos : MongoDB, NodeJs, ExpressJs
 * Author : Kiruban PREMKUMAR
 **/

var express = require('express'),
user = require('./routes/users'),
movie = require('./routes/movies'),
stat = require('./routes/stats'),
expressValidator = require('express-validator');

var app = express();

app.configure(function (req,res) {
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser('jVdIHT4QIBC4VKXsRE8LjVdIHT4<<<<<KIRU42RULEZ>>>>>QIBC4VKXsRE8LjVdIHT4QIBC4VKXsRE8L'));
  app.use(express.session());
  app.use(expressValidator());
  app.use(app.router);
  app.use(require('stylus').middleware(__dirname + '/public'));
  app.use(express.static(__dirname + '/public'));
});

// Page d'accueil
app.get('/', user.index);

// Connexion
app.get('/login', user.login); // vue
app.post('/login', user.loginSubmit); // post

// Deconnexion
app.get('/logout', user.logout);

// Administration des utilisateurs
app.get('/users', user.findAll);
app.get('/register', user.registerView);
app.post('/register', user.registerUser);
app.get('/users/:id', user.updateView);
app.post('/users/:id', user.updateUser);
app.get('/delete/:id', user.deleteUser);


// Accèss utilisateur
app.post('/search', movie.search);
app.get('/movie/:id', movie.findOne);
app.get('/mymovies', movie.mymovies);
app.get('/buy/:id', movie.buy);

// Accèss Admin
app.get('/listmovies', movie.listmovies);
app.get('/addmovie', movie.addmovie);
app.post('/insertmovie', movie.insertmovie);

// Les Statistiques
app.get('/stats',stat.index);


 app.listen(app.get('port'));
 //console.log('Server is listening on port '+app.get('port')+'...');