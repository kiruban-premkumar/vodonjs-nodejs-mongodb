/**
 * File : genRapport.js
 * Subject : IBD2
 * Project : NOSQL
 * Technos : MongoDB, NodeJs, ExpressJs
 * Author : Kiruban PREMKUMAR
 **/

var markdownpdf = require("markdown-pdf")
  , fs = require("fs")

fs.createReadStream("rapport.md")
  .pipe(markdownpdf())
  .pipe(fs.createWriteStream("rapport.pdf"))

fs.createReadStream("README.md")
  .pipe(markdownpdf())
  .pipe(fs.createWriteStream("README.pdf"))