/**
 * File : dataImport.js
 * Subject : IBD2
 * Project : NOSQL
 * Technos : MongoDB, NodeJs, ExpressJs
 * Author : Kiruban PREMKUMAR
 **/

var mongo = require('mongodb'),
Server = mongo.Server,
Db = mongo.Db,
BSON = mongo.BSONPure,
server = new Server('localhost', 27017, {auto_reconnect: true}),
db = new Db('test', server, {safe: true});

db.open(function(err, db) {
	db.collection('users', {w:0}, function(err, collection) {
	    collection.drop();
	});
	db.collection('movies', {w:0}, function(err, collection) {
	    collection.drop();
	});
    if(!err) {
        console.log("Connecté à la base test.");
        db.collection('movies', {strict:true, safe:true}, function(errCol, collection) {
            if (errCol) {
                console.log("La collection movies n'existe pas. On crée...");
                createCollectionMovies();
                createCollectionUsers();
            }
            else
            {
            	console.log("La collection movies existe déjà.....");
            }
        });
    } else {
      console.log("Pas de base de données test!");
  }
});


var createCollectionMovies = function() {
    db.createCollection("movies", function(err, collection){
    });
    insertMovieData();
}
var insertMovieData = function() {

    var movies = [
    {
        titre: "Iron Man",
        genre: "Action",
        realisateur: ["Jon Favreau"],
        acteurs: ["Robert Downey Jr.", "Terrence Howard", "Gwyneth Paltrow", "Jeff Bridges"],
        producteurs: ["Paramount Pictures","Marvel Studios"],
        scenaristes: ["Matt Holloway","Art Marcum","Mark Fergus"],
        annee_sortie: 2008,
        langue: 'Français',
        pays_production: 'Etats-Unis',
        synopsys: "Tony Stark, playboy, milliardaire, n'est pas seulement l'héritier des usines d'armement de son père, c'est également un inventeur de génie. Alors qu'il est en déplacement en Afghanistan afin de présenter sa dernière création, le missile Jéricho, il est enlevé par des terroristes. Gravement blessé lors de l'attaque, il ne survit que grâce à un électro-aimant placé près de son cœur et alimenté par une batterie de voiture. Ne cédant pas aux menaces des terroristes qui veulent lui faire reproduire son missile, il fabrique, à la place et dans le plus grand secret, un générateur ARK miniaturisé pour remplacer la batterie de voiture et une armure d'acier truffée de gadgets, grâce à laquelle il réussit à s'enfuir. Profondément marqué par cet enlèvement, et constatant que les terroristes possèdent des armes qu'il a lui-même conçues, il décide de donner un nouveau tournant à sa vie en mettant le potentiel de son armure au service de la protection des innocents et de la justice.",
        users: [],
        created: Date.now()
    },
    {
        titre: "Rush",
        genre: "Drame",
        realisateur: ["Ron Howard"],
        acteurs: ["Chris Hemsworth", "Daniel Brühl", "Olivia Wilde", "Alexandra Maria Lara"],
        producteurs: ["Cross Creek Pictures","Egoli Tossell Film","Exclusive Media Group"],
        scenaristes: ["Peter Morgan"],
        annee_sortie: 2013,
        langue: 'Français',
        pays_production: 'États-Unis',
        synopsys: "Le Britannique James Hunt et l'Autrichien Niki Lauda, deux talentueux pilotes, se rencontrent au début des années 1970, alors que tous deux courent en Formule 3 et ont en point commun leur passion de la course tout en ayant des personnalités diamétralement opposées : le premier, un personnage « décalé, fêtard, trompe-la-mort », qui multiplie les conquêtes féminines et le deuxième, un pilote sérieux et très professionnel qui évalue la limite de risques à ne pas dépasser, « 20 % ». Pour ces raisons, ils affichent un grand mépris mutuel.",
        users: [],
        created: Date.now()
    },
    {
        titre: "Ted",
        genre: "Comedie",
        realisateur: ["Seth MacFarlane"],
        acteurs: ["Mark Wahlberg","Mila Cunis",""],
        producteurs: ["Media Rights Capital","Smart Entertainment"],
        scenaristes: ["Seth MacFarlane","Alec Sulkin","Wellesley Wild","Seth MacFarlane"],
        annee_sortie: 2012,
        langue: 'Anglais',
        pays_production: 'Etats-Unis',
        synopsys: "En 1985, John Bennet, un garçon de huit ans, n'arrive pas à se faire d'amis. Le soir de Noël, il fait le vœu que son ours en peluche qu'il vient de recevoir à Noël prenne vie et qu'il soit son meilleur ami pour la vie ; son vœu sera exaucé par magie, l'ours Ted (traduction de « Teddy bear ») prend vie et cette histoire devient célèbre",
        users: [],
        created: Date.now()
    },
    {
        titre: "The Wolf of Wall Street",
        genre: "Action",
        realisateur: ["Martin Scorsese"],
        acteurs: ["Leonardo DiCaprio","Leonardo DiCaprio","Kyle Chandler"],
        producteurs: ["Appian Way","EMJAG Productions"],
        scenaristes: ["Terence Winter"],
        annee_sortie: 2014,
        langue: 'Anglais',
        pays_production: 'Etats-Unis',
        synopsys: "Jordan Belfort est un courtier en bourse dans les années 80 et 90. Alors âgé de trente ans il monte sa propre boîte Stratton Oakmont , mais va vite tomber dans tous les excès : L'argent gagné par la fraude, prostituées et consommation de drogues à outrance...Il vivra cette vie pendant dix ans avant d'être arrêté par le FBI, et condamné à 36 mois de prison, mais ayant accepté de collaborer avec le FBI il ne passera que 22 mois en prison.",
        users: [],
        created: Date.now()
    },
    {
        titre: "Very bad trip",
        genre: "Comedie",
        realisateur: ["Todd Phillips"],
        acteurs: ["Alan Garner", "Ed Helms", "Ken JYONG", "justin barthra"],
        producteurs: ["Daniel Goldberg","Todd Phillips"],
        scenaristes: ["Scott Moore"],
        annee_sortie: 2012,
        langue: 'Anglais',
        pays_production: 'États-Unis',
        synopsys:"Doug (Justin Bartha) s'apprête à se marier, alors ses amis Phil (Bradley Cooper), un professeur qui en a marre de la vie de marié, Stu (Ed Helms) dentiste qui s'apprête à faire sa demande à sa stricte et sévère copine depuis trois ans, et le futur beau-frère de Doug, Alan (Zach Galifianakis) g",
        users: [],
        created: Date.now()
    },
    {
        titre: "Frankenstein",
        genre: "Action",
        realisateur: ["Chris Pine"],
        acteurs: ["Ludovic chatelle", "Chris Pine"],
        producteurs: ["Albert Entertainment"],
        scenaristes: ["Jack Ryan ","Keira Knightley"],
        annee_sortie: 2013,
        langue: 'Anglais',
        pays_production: 'États-Unis',
        synopsys:"Jack Ryan vient juste de quitter les Marines et s'apprète à rejoindre la CIA. Ryan travaille alors comme consultant financier pour un milliardaire russe. Il va être impliqué dans un complot terroriste.",
        users: [],
        created: Date.now()
    },
    {
        titre: "Fast and Furious 6",
        genre: "Action",
        realisateur: ["Justin Lin"],
        acteurs: ["Vin diesel ", "Paul Walker"],
        producteurs: ["Original Film"],
        scenaristes: ["Chris Morgan "],
        annee_sortie: 2013,
        langue: 'Anglais',
        pays_production: 'États-Unis',
        synopsys:"Dominic Toretto, Brian O'Conner et toute leur équipe, après le casse de Rio, ayant fait tomber un empire en empochant 100 millions de dollars, se sont dispersés aux quatre coins du globe. Mais l’incapacité de rentrer chez eux, et l’obligation de vivre en cavale permanente, laissent à leur vie le goût amer de l’inaccomplissement. Pendant ce temps Luke Hobbs traque aux quatre coins du monde un groupe de chauffeurs mercenaires aux talents redoutables, dont le meneur, Owen Shaw est secondé d’une main de fer par l’amour que Dominic croyait avoir perdu pour toujours : Leticia Ortiz. La seule façon d’arrêter leurs agissements est de les détrôner en surpassant leur réputation. Luke demande donc à Dominic de rassembler son équipe de choc à Londres. En retour ? Ils seront tous graciés et pourront retourner auprès des leurs, afin de vivre une vie normale.",
        users: [],
        created: Date.now()
    },
    
    {
        titre: "Taken",
        genre: "Action",
        realisateur: ["Olivier Megaton"],
        acteurs: ["Liam Neeson", "Famke Janssen","Maggie Grace"],
        producteurs: ["EuropaCorp"],
        scenaristes: ["Luc Besson ","Robert Mark Kamen"],
        annee_sortie: 2012,
        langue: 'Anglais',
        pays_production: 'FRANCE',
        synopsys:"Un an après avoir réussi à reprendre sa fille aux mains de la mafia albanaise, Bryan Mills, ex-agent de la CIA, aujourd'hui en vacances en famille à Istanbul, doit faire face à Murad, le chef du clan. Ce dernier réclame vengeance, après la mort de son fils.",
        users: [],
        created: Date.now()
    },
    {
        titre: "Never Back Down ",
        genre: "Drame",
        realisateur: ["Jeff Wadlow"],
        acteurs: ["Sean Faris", "Amber Heard"],
        producteurs: ["LA Entertainment"],
        scenaristes: ["Chris Hauty "],
        annee_sortie: 2008,
        langue: 'Anglais',
        pays_production: 'États-Unis',
        synopsys:"Jake suit son frère et sa mère, partis s’installer à Orlando en Floride. Il vient d’arrêter le football américain : trop impulsif, trop violent. La vidéo de son dernier combat circule sur Internet et attire l’attention de Ryan McCarthy, jeune riche imbu de sa personne et champion de MMA, des arts martiaux mixtes. Ce dernier propose à Jake un combat. Par orgueil, Jake accepte mais perd la manche. Il décide d’apprendre à se battre avec Jean Roqua, un étrange coach qui ne quitte jamais sa salle d’entraînement. Jake tombe amoureux de la petite amie de Ryan, Baja. Énervé, Ryan veut à tout prix affronter Jake à nouveau, cette fois-ci en compétition pour le Beat Down, où presque tous les coups sont permis.",
        users: [],
        created: Date.now()
    },
    {
        titre: "The Ryan Initiative ",
        genre: "Action",
        realisateur: ["Stuart Beattie"],
        acteurs: ["Kevin Grevioux", "EStuart Beattie"],
        producteurs: ["Jai Courtney"],
        scenaristes: ["Scott Lionnel"],
        annee_sortie: 2013,
        langue: 'Anglais',
        pays_production: 'États-Unis',
        synopsys:"La rencontre entre Frankenstein et d'autres célèbres créatures, telles que Dracula, le Bossu de Notre Dame ou encore l'Homme Invisible.",
        users: [],
        created: Date.now()
    }
    ];

    db.collection('movies', function(err, collection) {
        collection.insert(movies, {safe:true}, function(err, result) {
            console.log(result);
            if (result) console.log("Films ajoutés. :-)");
        });
    });

};





var createCollectionUsers = function() {
    db.createCollection("users", function(err, collection){
    });
    insertUserData();
}
var insertUserData = function() {

    var users = [
    {
        email: "sofiane.benbourahel@gmail.com",
        username: "souf",
        password: "souf123",
        movies : [],
        created: Date.now(),
        admin: false
    },
    {
        email: "kiru42@gmail.com",
        username: "kiru",
        password: "kiru42",
        movies: [],
        created: Date.now(),
        admin: true
    },
    {
        email: "admin@vod42.com",
        username: "admin",
        password: "admin",
        movies: [],
        created: Date.now(),
        admin: true
    },
    {
        email: "user@gmail.com",
        username: "user",
        password: "user",
        movies: [],
        created: Date.now(),
        admin: false
    },
    {
        email: "stephane@gmail.com",
        username: "stef",
        password: "stef",
        movies : [],
        created: Date.now(),
        admin: false
    },
    {
        email: "gilles@gmail.com",
        username: "gilles",
        password: "gilles",
        movies : [],
        created: Date.now(),
        admin: false
    },
    {
        email: "sylvain@gmail.com",
        username: "sylvain",
        password: "sylvain",
        movies : [],
        created: Date.now(),
        admin: false
    },
    {
        email: "claude@gmail.com",
        username: "claude",
        password: "claude",
        movies : [],
        created: Date.now(),
        admin: false
    },
    ];

    db.collection('users', function(err, collection) {
        collection.insert(users, {safe:true}, function(err, result) {
            console.log(result);
            if (result) console.log("Utilisateurs ajoutés. :-)");
        });
    });

};

//process.exit(0);