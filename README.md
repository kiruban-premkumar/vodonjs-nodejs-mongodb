
# DEMO

![demo](https://github.com/kiru42/VodOnJs/blob/master/public/images/demo/1.png?raw=true)
![demo](https://github.com/kiru42/VodOnJs/blob/master/public/images/demo/2.png?raw=true)
![demo](https://github.com/kiru42/VodOnJs/blob/master/public/images/demo/3.png?raw=true)
![demo](https://github.com/kiru42/VodOnJs/blob/master/public/images/demo/4.png?raw=true)
![demo](https://github.com/kiru42/VodOnJs/blob/master/public/images/demo/5.png?raw=true)
![demo](https://github.com/kiru42/VodOnJs/blob/master/public/images/demo/6.png?raw=true)

# README - Manuel d'utilisation

Il y a un rapport (rapport.md) qui explique les choix de modélisations et implémentations des requêtes.

# Application web

Les étapes pour tester l'application web.

## 0) Installation des modules 
	npm install

## 1) Créer une base de données test

## 2) importer les données :
	./import.sh
-> Méthode pratique pour les stats

## 2 bis) importer les données :
	node dataImport.js
-> Cette méthode à l'inconvéniant de ne pas avoir d'achats. Pas pratique pour les stats.

## 3) lancer le serveur
	node app.js

## 4) Tester l'application
	http://localhost:3000

# Les utilisateurs par défaut :

- Accèss administrateur => admin/admin
- Accèss client => user/user