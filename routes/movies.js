var mongo = require('mongodb'),
Server = mongo.Server,
Db = mongo.Db,
BSON = mongo.BSONPure,
server = new Server('localhost', 27017, {auto_reconnect: true}),
db = new Db('test', server, {safe: true});

db.open(function(err, db) {
    if(!err) {
        console.log("Connecté à la base test.");
        db.collection('movies', {strict:true, safe:true}, function(errCol, collection) {
            if (errCol) {
                console.log("La collection movies n'existe pas. Utilisez dataImport.js !");
            }
        });
    } else {
      console.log("Pas de base de données test!");
  }
});

exports.isLoggedIn = function(req, res) {
    if (typeof req.session.username !== 'undefined') {
        return true;
    }
    return false;
}

exports.isAdmin = function(req, res) {
    if (typeof req.session.admin !== 'undefined') {
        return req.session.admin;
    }
    return false;
}

exports.mymovies = function(req, res){
    if (!exports.isLoggedIn(req, res)) {
        return res.render('login', { user: req.session, loginErr: null, username: '' });
    }
    db.collection('movies', function(err, collection) {
        collection.find( { 'users.user': req.session._id }).toArray(function(err, movies) {
            var success = req.session.flash == true?true:false;
            res.render('mymovies', { movies: movies, user: req.session, success: success});
            req.session.flash = false;
        });
    });
};

exports.addmovie = function(req, res){
    if (!exports.isAdmin(req, res)) {
        return res.render('login', { user: req.session, loginErr: 'ADMIN ONLY !!!! :P', username: '' });
    } else {
        res.render('addmovie', {
            user: req.session,
            regErr: null,
            titre: '',
            genre: '',
            realisateur: '',
            acteurs: '',
            producteurs: '',
            scenaristes: '',
            annee_sortie: '',
            langue: '',
            pays_production: '',
            synopsys: ''
        });
    }
};

exports.insertmovie = function(req, res){
    if (!exports.isAdmin(req, res)) {
        return res.render('login', { user: req.session, loginErr: 'ADMIN ONLY !!!! :P', username: '' });
    }
    req.assert('titre', "Le titre ne peut être vide.").notEmpty();
    req.assert('genre', "Le genre ne peut être vide.").notEmpty();
    req.assert('realisateur', "Le realisateur ne peut être vide.").notEmpty();
    req.assert('acteurs', "Le champs acteurs ne peut être vide.").notEmpty();
    req.assert('producteurs', "Le champs producteurs ne peut être vide.").notEmpty();
    req.assert('scenaristes', "Le champs scenaristes ne peut être vide.").notEmpty();
    req.assert('annee_sortie', "Le champs annee de sortie ne peut être vide.").notEmpty();
    req.assert('langue', "Le champs langue ne peut être vide.").notEmpty();
    req.assert('pays_production', "Le champs pays_production ne peut être vide.").notEmpty();
    req.assert('synopsys', "Le champs synopsys ne peut être vide.").notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        return res.render('addmovie',
            { user: req.session, regErr: errors[0].msg, 
                titre: req.body.titre,
                genre: req.body.genre,
                realisateur: req.body.realisateur,
                acteurs: req.body.acteurs,
                producteurs: req.body.producteurs,
                scenaristes: req.body.scenaristes,
                annee_sortie: req.body.annee_sortie,
                langue: req.body.langue,
                pays_production: req.body.pays_production,
                synopsys: req.body.synopsys
            }
            );
    }

    db.collection('movies', function (err, collection) {
        collection.findOne({'titre':req.body.titre}, function(err, result) {
            if (result) {
                return res.render('addmovie',
                    { user: req.session,
                        regErr: "Ce film est déjà dans la base de données !",
                        titre: req.body.titre,
                        genre: req.body.genre,
                        realisateur: req.body.realisateur,
                        acteurs: req.body.acteurs,
                        producteurs: req.body.producteurs,
                        scenaristes: req.body.scenaristes,
                        annee_sortie: req.body.annee_sortie,
                        langue: req.body.langue,
                        pays_production: req.body.pays_production,
                        synopsys: req.body.synopsys
                    }
                    );
            } else {
                collection.insert({
                    titre: req.body.titre,
                    genre: req.body.genre,
                    realisateur: req.body.realisateur,
                    acteurs: req.body.acteurs,
                    producteurs: req.body.producteurs,
                    scenaristes: req.body.scenaristes,
                    annee_sortie: req.body.annee_sortie,
                    langue: req.body.langue,
                    pays_production: req.body.pays_production,
                    synopsys: req.body.synopsys,
                    users: [],
                    created: Date.now()
                }, {safe:true}, function (err, result) {
                    if (result) {
                        req.session.flash = true;
                        res.redirect('/listmovies');
                    }
                });
            }
        });
    });

};

// admin list movies
exports.listmovies = function(req, res){
    if (!exports.isAdmin(req, res)) {
        return res.render('login', { user: req.session, loginErr: 'ADMIN ONLY !!!! :P', username: '' });
    }
    db.collection('movies', function(err, collection) {
        collection.find().toArray(function(err, movies) {
            var success = req.session.flash == true?true:false;
            res.render('listmovies', { movies: movies, user: req.session, success: success});
            req.session.flash = false;
        });
    });
};

exports.search = function(req, res) {
    if (!exports.isLoggedIn(req, res)) {
        return res.render('login', { user: req.session, loginErr: null, username: '' });
    }

    var query = {};

    if(req.body.title != ''){
        query['titre'] = {};
        query['titre']['$regex'] = '.*' + req.body.title + '.*';
        query['titre']['$options'] = 'i';
    }
    if(req.body.date_start != '--' && req.body.date_end != '--'){
        query['annee_sortie'] =  {};
        query['annee_sortie']['$gte'] = parseInt(req.body.date_start);
        query['annee_sortie']['$lte'] = parseInt(req.body.date_end);
    }

    if(req.body.genre != '--'){
        query['genre'] =  {};
        query['genre']['$regex'] = req.body.genre;
    }

    if(req.body.persons != ''){
        var persons = req.body.persons.split(',');
        query['$or'] =  [];
        query['$or'].push({"realisateur": { $in: persons } });
        query['$or'].push({"acteurs": { $in: persons } });
        query['$or'].push({"producteurs": { $in: persons } } );
        query['$or'].push({"scenaristes": { $in: persons } } );
    }
    
    console.log(query);
    db.collection('movies', function(err, collection) {
        collection.find(query).limit(10).toArray(function(err, movies) {
            res.render('search', { movies: movies, user: req.session });
        });
    });
};

exports.findOne = function(req, res) {
    var id = req.params.id;
    if (!exports.isLoggedIn(req, res)) {
        return res.render('login', { user: req.session, loginErr: null, username: '' });
    }
    db.collection('movies', function(err, collection) {
        collection.find( { 'users.user': req.session._id, '_id': new BSON.ObjectID(id) }).toArray(function(err, movies) {
            req.session.buy = false;
            if(movies.length == 0)
                req.session.buy = true;

            db.collection('movies', {w:0}, function(err, collection) {
                collection.findOne({'_id': new BSON.ObjectID(id) }, function(err, movie) {
                    //console.log(movie);
                    res.render('movie', { movie: movie, user: req.session });
                });
            });

        });
    });
};


exports.buy = function(req, res) {
    var id = req.params.id;
    var date_achat = new Date();
    if (!exports.isLoggedIn(req, res)) {
        return res.render('login', { user: req.session, loginErr: null, username: '' });
    }

    db.collection('movies', {w:0}, function(err, collection) {
        collection.findOne({'_id': new BSON.ObjectID(id) }, function(err, movie) {
            db.collection('movies', function(err, collection) {
                collection.update({'_id':new BSON.ObjectID(id)}, { $addToSet: { users: { date: date_achat, user: req.session._id } } }, {safe:true}, function(err, result) {
                        if (err) {
                            console.log('Error buying movie: ' + err);
                            res.send({'error':'An error has occurred'});
                        } else {
                            console.log('' + result + ' document(s) updated');
                            db.collection('users', function(err, collection) {
                                collection.update({'_id':new BSON.ObjectID(req.session._id)}, { $addToSet: { movies: { date: date_achat, movie: id, genre: movie.genre } } }, {safe:true}, function(err, result) {
                                    if (err) {
                                        console.log('Error buying movie: ' + err);
                                        res.send({'error':'An error has occurred'});
                                    } else {
                                        console.log('' + result + ' document(s) updated');
                                        req.session.flash = true;
                                        res.redirect('/mymovies');
                                    }
                                });
                            });
                        }
                    });
                });
        });
    });

};