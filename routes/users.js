var mongo = require('mongodb'),
Server = mongo.Server,
Db = mongo.Db,
BSON = mongo.BSONPure,
server = new Server('localhost', 27017, {auto_reconnect: true}),
db = new Db('test', server, {safe: true});

db.open(function(err, db) {
    if(!err) {
        console.log("Connected to your 'test' mongo database");
        db.collection('users', {strict:true, safe:true}, function(errCol, collection) {
            if (errCol) {
                console.log("La collection user n'existe pas... Utilisez dataImport.js!");
            }
        });
    } else {
        console.log("Pas de base test... ");
  }
});

exports.isAdmin = function(req, res) {
    if (typeof req.session.admin !== 'undefined') {
        return req.session.admin;
    }
    return false;
}
/*
*  Renders to index page before login.
* */
exports.index = function(req, res){
    if (exports.isAdmin(req, res)) {
        db.collection('movies', function(err, collection) {
            collection.find().toArray(function(err, movies) {
                res.render('listmovies', { movies: movies, user: req.session, success: false});
            });
        });
    } else if (exports.isLoggedIn(req, res)) {
        return res.render('home',
            { user: req.session, username: req.session.username }
            );
    } else {
        return res.render('login', { user: req.session, loginErr: null, username: '' });
    }
};

/*
*  Renders to login page, if no user is logged.
* */
exports.login = function(req, res){
    if (typeof req.session.username == 'undefined') {
        res.render('login', { user: req.session, loginErr: null, username: '' });
    }
    else res.redirect('/');
};

/*
*  Submit the username and password for authorization
* */
exports.loginSubmit = function(req, res){

    req.assert('username', "Le nom d'utilisateur ne peut être vide!").notEmpty();
    req.assert('pwd', "Le mot de passe ne peut être vide!").notEmpty().len(1,16);

    var errors = req.validationErrors();
    if (errors) {
        return res.render('login',
            { user: req.session, loginErr: errors[0].msg, username: req.body.username }
            );
    }

    db.collection('users', function(err, collection) {
        collection.findOne({'username':req.body.username, 'password':req.body.pwd}, function(err, result) {
            if (result) {
                req.session.username = result.username;
                req.session.admin = result.admin;
                req.session._id = result._id;
                res.redirect('/');
            } else {
                res.render('login',
                    { user: req.session, loginErr: 'Identifiant ou mot de passe incorrect!', username: req.body.username }
                    );
            }
        });
    });

};

/*
*  Page d'inscription / la vue
* */
exports.registerView = function(req, res){
    if (typeof req.session.username != 'undefined') {
        res.render('home', { user: req.session });
    } else {
        res.render('register', {
            user: req.session,
            regErr: null,
            username: '',
            email: ''
        });
    }
};

/*
*  Page d'inscription / après submit / données post
* */
exports.registerUser = function(req, res){

    req.assert('email', 'Entrez une adresse mail valide').len(6,64).isEmail();
    req.assert('username', "Votre nom d'utilisateur ne peut être null!").notEmpty();
    req.assert('pwd', "Le mot de passe ne peut être vide et doit être de 6 - 16 caractères!").notEmpty().len(1,16);
    req.assert('confirmpwd', "La confirmation de mot de passe ne peut être vide").notEmpty();
    req.assert('pwd', 'Les mots de passes ne correspondent pas.').equals(req.param('confirmpwd'));

    var errors = req.validationErrors();
    if (errors) {
        return res.render('register',
            { user: req.session,  username: req.body.username, regErr: errors[0].msg, email: req.body.email }
            );
    }

    db.collection('users', function (err, collection) {
        collection.findOne({'email':req.body.email}, function(err, result) {
            if (result) {
                return res.render('register',
                    { user: req.session,  username: req.body.username, regErr: "L'adresse email est déjà utilisé!", email: req.body.email }
                    );
            } else {
                collection.insert({
                    email:req.body.email,
                    username:req.body.username,
                    password:req.body.pwd,
                    created: Date.now()
                }, {safe:true}, function (err, result) {
                    if (result) {
                        res.redirect('/login');
                    }
                });
            }
        });
    });

};

/*
*  Afficher la liste des utilisateurs
* */
exports.findAll = function(req, res) {
    if (!exports.isAdmin(req, res)) {
        return res.render('login', { user: req.session, loginErr: 'ADMIN ONLY !!!! :P', username: '' });
    }
    db.collection('users', function(err, collection) {
        collection.find().toArray(function(err, items) {
            res.render('users', { items: items, user: req.session, username: req.body.username });
        });
    });
};

/*
*  Mettre à jour un utilisateur / la vue
* */
exports.updateView = function(req, res) {
    if (!exports.isAdmin(req, res)) {
        return res.render('login', { user: req.session, loginErr: 'ADMIN ONLY !!!! :P', username: '' });
    }
    var id = req.params.id;
    db.collection('users', {w:0}, function(err, collection) {
        collection.findOne({'_id':new BSON.ObjectID(id)}, function(err, result) {
            if (result) {
                res.render('update', { id: id, email: result.email, username: result.username, user: req.session});
            }
        });
    });
}

/*
*  Mettre à jour un utilisateur / post
* */
exports.updateUser = function(req, res) {
    if (!exports.isAdmin(req, res)) {
        return res.render('login', { user: req.session, loginErr: 'ADMIN ONLY !!!! :P', username: '' });
    }
    var id   = req.params.id,
    user = req.body;

    req.assert('email', 'Entrez une adresse mail valide').len(6,64).isEmail();
    req.assert('username', "Votre nom d'utilisateur ne peut être null!").notEmpty();
    req.assert('pwd', "Le mot de passe ne peut être vide et doit être de 6 - 16 caractères!").notEmpty().len(6,16);
    req.assert('confirmpwd', "La confirmation de mot de passe ne peut être vide").notEmpty();
    req.assert('pwd', 'Les mots de passes ne correspondent pas.').equals(req.param('confirmpwd'));

    var errors = req.validationErrors();
    if (errors) {
        return res.render('update',
            { id: id, email: user.email, user: req.session, username: user.username, err: errors[0].msg  }
            );
    }

    console.log('Updating user: ' + id);
    console.log(JSON.stringify(user));
    db.collection('users', function(err, collection) {
        collection.update({'_id':new BSON.ObjectID(id)}, { email: user.email, username: user.username, password: user.pwd  }, {safe:true}, function(err, result) {
            if (err) {
                console.log('Error updating user: ' + err);
                res.send({'error':'An error has occurred'});
            } else {
                console.log('' + result + ' document(s) updated');
                res.render('update', {id: id, email: user.email, username: user.username, user: req.session, success: true, disable: true});
            }
        });
    });
}

/*
*  Effacer utilisateur
* */
exports.deleteUser = function(req, res) {
    if (!exports.isAdmin(req, res)) {
        return res.render('login', { user: req.session, loginErr: 'ADMIN ONLY !!!! :P', username: '' });
    }
    var id = req.params.id;
    console.log('Deleting user: ' + id);
    db.collection('users', function(err, collection) {
        collection.remove({'_id':new BSON.ObjectID(id)}, {safe:true}, function(err, result) {
            if (err) {
                res.send({'error':'Erreur - ' + err});
            } else {
                exports.findAll(req, res);
            }
        });
    });
}

exports.isLoggedIn = function(req, res) {
    if (typeof req.session.username !== 'undefined') {
        return true;
    }
    return false;
}

exports.logout = function(req, res){
    req.session.destroy();
    res.redirect('/');
};