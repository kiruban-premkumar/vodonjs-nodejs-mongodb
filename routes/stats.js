var mongo = require('mongodb'),
Server = mongo.Server,
Db = mongo.Db,
BSON = mongo.BSONPure,
server = new Server('localhost', 27017, {auto_reconnect: true}),
db = new Db('test', server, {safe: true});


db.open(function(err, db) {
    if(!err) {
        console.log("Connecté à la base test.");
        db.collection('movies', {strict:true, safe:true}, function(errCol, collection) {
            if (errCol) {
                console.log("La collection movies n'existe pas. Utilisez dataImport.js !");
            }
        });
    } else {
      console.log("Pas de base de données test!");
  }
});

exports.isAdmin = function(req, res) {
    if (typeof req.session.admin !== 'undefined') {
        return req.session.admin;
    }
    return false;
}

/*
 * GET Page de statistiques
 */

exports.index = function(req, res){
	if (!exports.isAdmin(req, res)) {
        return res.render('login', { user: req.session, loginErr: 'ADMIN ONLY !!!! :P', username: '' });
    }
	var today = new Date();

	var start = new Date(today.getFullYear(), today.getMonth(), 1);
	var end = new Date(today.getFullYear(), today.getMonth()+1, 0);
	var stat1, stat2, stat3, stat4;

	// stat 1
	db.collection('movies', function(err, collection) {
		var map = function() { emit(this.titre, this.users.length); };
    	var reduce = function(titre, count) { return Array.sum(count); };
		collection.mapReduce(
		    map,
		    reduce,
		    {
		        query: { "users.date": { $gte: start, $lte: end } },
		        out : "tmpStat1"
		    },
		    function(err, results) {
	    		db.collection('tmpStat1', function(err, collection) {
			        collection.find().sort({value:-1}).limit(5).toArray(function(err, stat) {
			           	stat1 = stat;
			           	// stat2
					  	db.collection('movies', function(err, collection) {
							var map = function() { emit(this.genre, this.users.length); };
					    	var reduce = function(genre, nb) { return Array.sum(nb); };
							collection.mapReduce(
							    map,
							    reduce,
							    {
							        query: { "users.date": { $gte: start, $lte: end } },
							        out : "tmpStat2"
							    },
							    function(err, results) {
							    	console.log(err);
							    	console.log(results);
						           	db.collection('tmpStat2', function(err, collection) {
								        collection.find().sort({value:-1}).limit(1).toArray(function(err, stat) {
								           	stat2 = stat;
								           	// stat3
										  	db.collection('users', function(err, collection) {
												var map = function() { 
													var key = this.movies.genre;
													var value = {
														count: 1,
														username: this.username,
														userid: this_id
													};
													emit(key, value ); 
												};
										    	var reduce = function(key, values) { 
										    		var reducedObject = {
										    			username: values.username,

										    		};
										    	};
												collection.mapReduce(
												    map,
												    reduce,
												    {
												        out : "tmpStat3"
												    },
												    function(err, results) {
												    	console.log(err);
												    	console.log(results);
											           	db.collection('tmpStat3', function(err, collection) {
													        collection.find().sort({value:-1}).toArray(function(err, stat) {
													           	stat3 = stat;
													           	console.log(stat3);
													           	res.render('stats', { user: req.session, st1: stat1, st2: stat2, st3: stat3 });
													        });
													    });
												    }
												);
										   	});	
								        });
								    });
							    }
							);
					   	});	           	
			        });
			    });
		    }
		);
   	});

};